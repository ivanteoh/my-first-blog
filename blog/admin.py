"""
Blog admin.

This script is Django blog admin.
"""
from blog.models import Comment
from blog.models import Post
from django.contrib import admin

admin.site.register(Post)
admin.site.register(Comment)
