"""
Blog models.

This script is Django blog models.
"""

from django.db import models
from django.utils import timezone


class Post(models.Model):

    """Post class."""

    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        """Publish method."""
        self.published_date = timezone.now()
        self.save()

    def approved_comments(self):
        """Approved comments method."""
        return self.comments.filter(approved=True)

    def __str__(self):
        """String method."""
        return self.title


class Comment(models.Model):

    """Comment class."""

    post = models.ForeignKey('blog.Post', related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved = models.BooleanField(default=False)

    def approve(self):
        """Approve method."""
        self.approved = True
        self.save()

    def __str__(self):
        """String method."""
        return self.text
