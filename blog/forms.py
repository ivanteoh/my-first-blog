"""
Blog models.

This script is Django blog models.
"""
from django import forms
from blog.models import Comment
from blog.models import Post


class PostForm(forms.ModelForm):

    """Post form class."""

    class Meta:

        """Meta class."""

        model = Post
        fields = ('title', 'text',)


class CommentForm(forms.ModelForm):

    """Comment form class."""

    class Meta:

        """Meta class."""

        model = Comment
        fields = ('author', 'text',)
